package java_assignment_arrayoopsencpasulation;

public class JaggedArray {
    public static void main(String[] args) {
        int[][] zaggedArray = {{1, 2, 3, 5},
                {1, 2, 3},
                {1, 2, 3, 4, 5, 6}};
        int[][] zaggedArray1 = {{1, 2, 3},
                {1, 2},
                {1, 2, 3, 4, 5}};
        int res[][] = new int[3][];
        for (int i = 0; i < res.length; i++) {
            int max = Integer.max(zaggedArray[i].length, zaggedArray1[i].length);
            res[i] = new int[max];
            for (int j = 0; j < res[i].length; j++) {
                int min = Integer.min(zaggedArray[i].length, zaggedArray1[i].length);
                if (j < min) {
                    res[i][j] = zaggedArray1[i][j] + zaggedArray[i][j];
                } else if (zaggedArray[i].length > zaggedArray1[i].length) {
                    res[i][j] = zaggedArray[i][j];
                } else {
                    res[i][j] = zaggedArray1[i][j];
                }
            }
        }
        for (int[] i : res) {
            for (int j : i) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}
