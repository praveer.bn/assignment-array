package java_assignment_arrayoopsencpasulation;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class NthArray {
    public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the index to be searched.");
                int i = sc.nextInt();

                int arr[] = {1,2,3,4,5,6,7,8,9,10};

                int n = Array.getInt(arr,i);

                System.out.println("Value present at index "+i+" is "+n);
            }
        }


